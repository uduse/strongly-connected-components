#pragma once
#include "Edge.h"
#include <vector>
#include <iostream>
#include <iomanip>
#include <stack>

using namespace std;

class Node
{
public:
	Node();
	~Node();
	int index;
	int time; // Finishing Time
	int group; // Strongly Connected Groups
	bool visited;

	// DFS
	void assignTime();
	void assignGroup( int groupIndex );
	vector<int> adjacent;
	vector<int> reverse_adjacent;




	static void timeAll();
	static void sortTime();
	static int compareTime( const void* A, const void* B );

	static void initVisit();

	static void groupAll();
	static void countLargestGroups();

	static void print();
	static void printGroups();

	static Node* map;
	static int mapSize;
	static int numGroups;
	static stack<Node*> todo;

	//static vector<Edge> street;
	//static vector<Edge> reversed_street;

private:

};

