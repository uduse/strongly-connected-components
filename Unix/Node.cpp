//#include "stdafx.h"
#include "Node.h"
#include <algorithm>
#include <functional>
//#include <xfunctional>

Node* Node::map;
int Node::mapSize;
stack<Node*> Node::todo;
int Node::numGroups;

//vector<Edge> Node::reversed_street;

Node::Node():index( 0 ), time( 0 ), group( 0 ), visited( false )
{
	static int count = 1;
	index = count;
	count++;
}

Node::~Node()
{
}

void Node::timeAll()
{
	for ( int i = 0; i < mapSize; i++ )
	{
		if ( !map[i].visited )
		{
			Node::map[i].assignTime();
		}
	}
}

void Node::assignTime()
{
	static int count = 1;

	visited = true;
	for ( int i = 0; i < reverse_adjacent.size(); i++ )
	{
		if ( !map[reverse_adjacent[i]].visited )
		{
			map[reverse_adjacent[i]].assignTime();
		}
	}

	time = count;
	count++;
	todo.push( this );
	return;
}

void Node::groupAll()
{
	int groupIndex = 0;
	//int count = 0;
	while ( !todo.empty() )
	{
		if ( !todo.top()->visited )
		{
			todo.top()->assignGroup( groupIndex );
			groupIndex++;
		}
		todo.pop();
		//count++;
	}
	numGroups = groupIndex - 1;
}

void Node::assignGroup( int groupIndex )
{
	visited = true;
	group = groupIndex;

	for ( int i = 0; i < adjacent.size(); i++ )
	{
		if ( !map[adjacent[i]].visited )
		{
			map[adjacent[i]].assignGroup( groupIndex );
		}
	}
}

int Node::compareTime( const void* A, const void* B )
{
	// Put larger one in front.

	Node* nodeA = (Node*)A;
	Node* nodeB = (Node*)B;

	if ( nodeA->time > nodeB->time )
	{
		return -1;
	}
	else if ( nodeA->time < nodeB->time )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void Node::initVisit()
{
	for ( int index = 0; index < mapSize; index++ )
	{
		map[index].visited = false;
	}
}

void Node::sortTime()
{
	qsort( map, mapSize, sizeof( Node ), compareTime );
}

void Node::print()
{
	for ( int i = 0; i < mapSize; i++ )
	{
		cout << "[ " << setw( 3 ) << map[i].index << "]";
		cout << " <" << setw( 3 ) << map[i].group << ">";
		cout << "    @" << setw( 3 ) << map[i].time;
		cout << "\t\t>> ";
		for ( int j = 0; j < map[i].adjacent.size(); j++ )
		{
			cout << map[map[i].adjacent[j]].index << " ";
		}
		cout << endl;
	}
}

void Node::printGroups()
{
	cout << endl;
	int groupCount = 0;
	while ( true )
	{
		cout << "#" << groupCount << " >> ";
		for ( int index = 0; index < mapSize; index++ )
		{
			if ( map[index].group == groupCount )
			{
				cout << map[index].index << ", ";
			}
		}

		if ( groupCount == numGroups )
		{
			break;
		}

		groupCount++;
		cout << endl;
	}

	cout << endl;
}

int compare( const void * a, const void * b )
{
	return ( *(int*)b - *(int*)a );
}

void Node::countLargestGroups()
{
	int* groupSize = new int[numGroups + 1]();
	for ( int index = 0; index < mapSize; index++ )
	{
		groupSize[map[index].group]++;
	}
	//cout << "F" << endl;
	//sort( groupSize, groupSize + numGroups, greater<int>() );
	//cout << "numGroups: " << numGroups << endl;
	qsort( groupSize, numGroups + 1, sizeof( int ), compare );

	////cout << "F2" << endl;
	cout << "Top 5 Size: " << endl;
	//cout << "numGroups: " << numGroups << endl;
	for ( int count = 0; count < 5 && count < numGroups + 1; count++ )
	{
		cout << groupSize[count] << " ";
	}
	cout << endl; cout << endl;
}