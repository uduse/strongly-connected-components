##Strongly-Connected-Components

Implementation of an algorithm that computes SCC (Strongly Connected Components) of a graph.

##Things I learned from this project
1. **Stack Overflow!** The first time in my life encountering this problem not because of bugs. The input file (i.e. the graph I am operating on) was super large, and I was using recursive function calls to solve the problem. The call stack was just not large enough. Solved this by two means: **Uploading my codes to a server on campus that has better performance**, and **Reserving more space for stack in Visual Studio.**
