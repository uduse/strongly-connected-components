// Strongly Connected Components.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <fstream>
#include <iostream>
#include "Node.h"
#include "Edge.h"
#include <time.h>
#include <vector>
#include <stdlib.h>
//#include <functional>
#include <xfunctional>

using namespace std;

/*
 * File 1
 * Answer: 3,3,3,0,0
 *
 *
 **/



int _tmain( int argc, _TCHAR* argv[] )
{

	//====================================//
	//  Init
	//====================================//

	clock_t begin = clock();
	int source;
	int sink;

	//const int inputSize = 9;
	//const int inputSize = 11;
	//const int inputSize = 12;
	const int inputSize = 875714;
	//const int inputSize = ;

	//fstream input( "1.txt" );
	//fstream input( "2.txt" );
	//fstream input( "3.txt" );
	fstream input( "SCC.txt" );
	Node::map = new Node[inputSize];
	Node::mapSize = inputSize;


	int recordSource = -1;
	while ( input >> source >> sink )
	{
		if ( source % ( ( inputSize / 100 ) + 1 ) == 0 && recordSource != source )
		{
			cout << "Reading... " << int( source * 100 / inputSize ) << "%" << endl;
			recordSource = source;
		}
		source--;
		sink--;
		Node::map[source].adjacent.push_back( sink );
		Node::map[sink].reverse_adjacent.push_back( source );
		//Node::street.push_back( Edge( temp, temp2 ) );
		//Node::reversed_street.push_back( Edge( temp2, temp ) );
	}



	//====================================//
	//  Pass 1
	//====================================//
	cout << "Pass 1 ... ";
	Node::timeAll();
	//cout << "F" << endl;
	//Node::sortTime();
	Node::initVisit();
	//cout << "F" << endl;
	cout << "Done" << endl;

	//====================================//
	//  Pass 2
	//====================================//
	cout << "Pass 2 ... ";
	Node::groupAll();
	cout << "Done" << endl;
	cout << endl;




	//cout << "F" << endl;

	//for ( int index = 0; index < temp; index++ )
	//{
	//	cout << Node::map[index].index << endl;
	//}

	Node::countLargestGroups();


	//Node::print();
	//Node::printGroups();

	clock_t end = clock();
	double elapsed_secs = double( end - begin ) / CLOCKS_PER_SEC;
	cout << endl;
	cout << "Running Time: " << elapsed_secs; cout << endl;
	exit( 0 );
	return 0;
}

